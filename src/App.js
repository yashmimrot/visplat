import React, { Component } from 'react';
import axios from 'axios';
import './App.css';
import Header from './header';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button } from 'react-bootstrap';


class App extends Component{
  constructor(props) {
    super(props)
  
    this.state = {
       vNum:"",
       rDate:""
    }
  }

  valueChanged = (event) =>
  {
    this.setState(
      {
        [event.target.id] : event.target.value
      }
    )
  }
  
  formUpload = () => {
    console.log(this.state.vNum)
    console.log(this.state.rDate)
    axios.get("http://localhost:8081/health").then((res) => console.log(res)).catch((err) => console.log(err))
  }
  render() {
    return(
      
      <div className='bg'>
        <center>
          <div className='App-header'><Header/><br/></div>
            <form className='text1' >
              <h2>Enter Vehicle Number and Reported Date</h2>
              <br/><label htmlFor="vehicleNum">Vehicle Number : </label>
              <input type="text" onChange={this.valueChanged} value={this.vNum} id="vNum" name="vehicleNum" placeholder="e.g. KA-09-JD-2342"></input><br/><br/><br/>

              <label htmlFor="reportedDate">Reported Date : </label>
              <input type="date" id="rDate" onChange={this.valueChanged} value={this.rDate} name="registeredDate" placeholder="Enter Reported Date"></input><br/><br/>
              <Button onClick={this.formUpload} className="btn btn-primary btn-lg active">SUBMIT</Button>  
            </form>
        </center>
      </div>
    )
  }
}

export default App;
